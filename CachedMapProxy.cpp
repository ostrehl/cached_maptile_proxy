#include "CachedMapProxy.h"
#include <QDebug>
#include <QImage>
#include <QBuffer>

CachedMapProxy::~CachedMapProxy()
{

}

void CachedMapProxy::requestTile(const QString map_type, const QString map_url, int zoom, int x, int y, HttpReply &reply)
{
    QMutexLocker locker(&m_mutex);
    emit loadTileAsynch(m_requestId, map_type, map_url, zoom, x, y);

    //wait for cache
    m_waitcond.wait(&m_mutex, 10000);

    //load image
    QImage tile(m_fileName);
    if(tile.isNull())
        return;

    QByteArray ba;
    QBuffer buffer(&ba);
    buffer.open(QIODevice::WriteOnly);

    if(m_fileName.contains(".png"))
    {
        tile.save(&buffer, "PNG"); // writes image into ba in PNG format
        reply.appendHeaderEntry("Content-Type", "image/png");
    }
    else if (m_fileName.contains(".jpg"))
    {
        tile.save(&buffer, "JPG"); // writes image into ba in PNG format
        reply.appendHeaderEntry("Content-Type", "image/jpg");
    } else
    {
        buffer.close();
        return;
    }


    reply.appendContent(ba);
    reply.setStatusCode(HttpReply::OK);
}

void CachedMapProxy::tileLoaded(const int requestId, const QString filename)
{
    QMutexLocker locker(&m_mutex);
    //qDebug() << "tile loaded for: " << requestId;
    if(m_requestId == requestId)
    {
        m_fileName = filename;
        m_waitcond.wakeAll();
    }

}

CachedMapProxy::CachedMapProxy(int requestId, QObject *parent) :
    QObject(parent),
    m_requestId(requestId)
{

}
