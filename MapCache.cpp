#include "MapCache.h"
#include <QDebug>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QImage>
#include <Settings.h>


MapCache::MapCache(QObject *parent) :
    QObject(parent),
    m_networkAccessManager(NULL)
{
    m_cacheDir.setPath(QDir::currentPath());
    if (!m_cacheDir.exists("cache_dir"))
        m_cacheDir.mkdir("cache_dir");
    m_cacheDir.cd("cache_dir");

    m_cachedTiles = m_cacheDir.entryInfoList().size();
}

void MapCache::loadTileAsynch(const int request_id, QString map_type, QString map_url,
                              const int zoom, const int x, const int y)
{
    //!NOTE QNetworkAccessManager must be created after event loop start
    if(m_networkAccessManager == NULL)
    {
        m_networkAccessManager = new QNetworkAccessManager(this);
        connect(m_networkAccessManager, SIGNAL(finished(QNetworkReply*)),
                this, SLOT(replyFinished(QNetworkReply*)), Qt::QueuedConnection);
    }

    QString tileName = getTileName(map_type,zoom,x,y,map_url);
    //check if im cache, else load from server

    //tile is already cached
    if(m_cacheDir.exists(tileName)){
        qDebug() << "cache hit";
        emit tileLoaded(request_id,
                        m_cacheDir.absoluteFilePath(tileName));
        return;
    }

    //load tile from server
    qDebug() << "cache miss";
    //prepare URL
    map_url.replace("%z", QString::number(zoom));
    map_url.replace("%x", QString::number(x));
    map_url.replace("%y", QString::number(y));

    //only request once
    if(!m_openRequestHash.contains(map_url))
        m_networkAccessManager->get(QNetworkRequest(QUrl(map_url)));

    //insert request into open request hash
    m_openRequestHash.insertMulti(map_url, request_id);
    m_urlTileNameHash.insert(map_url, tileName);
}

void MapCache::replyFinished(QNetworkReply * networkReply)
{

    QString url = networkReply->url().toString();
    QList<int> requests = m_openRequestHash.values(url);
    m_openRequestHash.remove(url);
    QString tileName = m_urlTileNameHash.take(url);

    //insert tile into cache
    if(networkReply->error() == QNetworkReply::NoError &&
            networkReply->attribute(QNetworkRequest::HttpStatusCodeAttribute ).toUInt() == HttpReply::OK
               )
    {
        QImage img;
        if (img.loadFromData(networkReply->readAll()))
        {
            img.save(m_cacheDir.absoluteFilePath(tileName));
            m_cachedTiles++;
            if(m_cachedTiles > Settings::MAX_TILES_IN_CACHE)
                cleanCache();
        }
        else
        {
            qDebug() << "error loading image";
        }

    }
    foreach (int requestId, requests)
    {
        emit tileLoaded(requestId, m_cacheDir.absoluteFilePath(tileName));
    }

    networkReply->deleteLater();
}

void MapCache::cleanCache()
{
    QFile file;
    QFileInfoList list = m_cacheDir.entryInfoList(QDir::Files, QDir::Time);
    for(int i = 0; i < Settings::DELETE_TILES_ON_OVERFLOW; i++)
    {
        file.setFileName(list.takeFirst().absoluteFilePath());
        file.remove();
        m_cachedTiles --;
    }
}

const QString MapCache::getTileName(const QString map_type, const int zoom, const int x, const int y, const QString map_url)
{
    QString imageFormat;
    if(map_url.contains(".png"))
        imageFormat = QString(".png");
    if(map_url.contains(".jpg"))
        imageFormat = QString(".jpg");

    return QString(map_type).append("_").append(QString::number(zoom))
            .append("_").append(QString::number(x)).append("_")
            .append(QString::number(y).append(imageFormat));
}
