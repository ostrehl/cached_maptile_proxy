#-------------------------------------------------
#
# Project created by QtCreator 2015-04-01T14:01:14
#
#-------------------------------------------------

QT       += core network

TARGET = cached_map_proxy
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    webserver/WebSocketRunnable.cpp \
    webserver/WebServer.cpp \
    Dispatcher.cpp \
    CachedMapProxy.cpp \
    webserver/HttpReply.cpp \
    MapCache.cpp

HEADERS += \
    webserver/WebSocketRunnable.h \
    webserver/WebServer.h \
    CachedMapProxy.h \
    Dispatcher.h \
    webserver/HttpReply.h \
    MapCache.h \
    Settings.h
