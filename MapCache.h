/**
    @file
    @copyright
        Copyright Olav Strehl 2015
        Distributed under the Boost Software License, Version 1.0.
        (See accompanying file LICENSE_1_0.txt or copy at
        http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef MAPCACHE_H
#define MAPCACHE_H

#include <QObject>
#include "webserver/HttpReply.h"
#include <QNetworkAccessManager>
#include <QMultiHash>
#include <QDir>

class MapCache : public QObject
{
    Q_OBJECT
public:
    explicit MapCache(QObject *parent = 0);

signals:
    void tileLoaded(int request_id, const QString fileName);

public slots:
    void loadTileAsynch(const int request_id, QString map_type, QString map_url, const int zoom, const int x, const int y);

private slots:
    void replyFinished(QNetworkReply *);

    void cleanCache();


private:
    const QString getTileName(const QString map_type, const int zoom, const int x, const int y, const QString map_url);

    QNetworkAccessManager * m_networkAccessManager;
    QMultiHash<QString, int> m_openRequestHash;
    QHash<QString, QString> m_urlTileNameHash;
    QDir m_cacheDir;
    qint32 m_cachedTiles;
};

#endif // MAPCACHE_H
