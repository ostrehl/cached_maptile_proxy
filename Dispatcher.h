/**
    @file
    @copyright
        Copyright Olav Strehl 2015
        Distributed under the Boost Software License, Version 1.0.
        (See accompanying file LICENSE_1_0.txt or copy at
        http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef DISPATCHER_H
#define DISPATCHER_H

#include <QObject>
#include "webserver/HttpReply.h"
#include <QMutex>
#include <QHash>
#include "MapCache.h"
#include "QThread"

class Dispatcher : public QObject
{
    Q_OBJECT

public:    
    static Dispatcher *getInstance();
    ~Dispatcher();


    void processRequest(QStringList &tokens, HttpReply &reply);

signals:

public slots:

private:
    explicit Dispatcher(QObject *parent = 0);
    QMutex m_mutex;
    int m_requestId;
    mutable QHash<QString, QString> m_mapTypeToUrl;
    MapCache m_mapChache;


};

#endif // DISPATCHER_H
