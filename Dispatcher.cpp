#include "Dispatcher.h"
#include "QDebug"
#include "QStringList"
#include "math.h"
#include "CachedMapProxy.h"
#include "Settings.h"



Dispatcher * Dispatcher::getInstance(){
    static Dispatcher * s_pInstance;
    static QMutex s_mutex;
    if(s_pInstance == NULL){
        QMutexLocker locker(&s_mutex);
        Q_UNUSED(locker);
        if(s_pInstance == NULL)
            s_pInstance = new Dispatcher();
    }
    return s_pInstance;

}

Dispatcher::~Dispatcher()
{

}

void Dispatcher::processRequest(QStringList &tokens, HttpReply & reply)
{

    //expected request: http://{xxx.xxx.xxx.xxx:xxxx}/tiles/{type}/6/37/21.png
    //("GET", "osmde", "6", "37", "21.png", "HTTP", "1.1", "")
    //qDebug() << tokens;

    if(tokens.size() != 8 )
    {
        reply.setStatusCode(HttpReply::NOT_FOUND);
        return;
    }

    QString type = tokens.at(1);
    int zoom = tokens.at(2).toInt();
    int x = tokens.at(3).toInt();
    QString sy = tokens.at(4);
    sy.chop(4);
    int y = sy.toInt();
    int maxNumTiles = pow(2,zoom);

    //catch invalid requests
    if (zoom < 0 || zoom > 19 || x < 0 || y < 0 || x > maxNumTiles || y > maxNumTiles )
    {
        qDebug() << "invalid parameters";
        reply.setStatusCode(HttpReply::NOT_FOUND);
        return;
    }

    int requestId;
    QString mapUrl;
    {
        QMutexLocker locker (&m_mutex);
        requestId = m_requestId++;

        mapUrl = m_mapTypeToUrl.value(type);
        if(mapUrl=="")
        {
            reply.setStatusCode(HttpReply::NOT_FOUND);
            qDebug() << "map type not found";
            return;
        }
    }

    CachedMapProxy * cachedMapProxy = new CachedMapProxy(requestId);
    connect(cachedMapProxy, SIGNAL(loadTileAsynch(int, QString, QString,int,int,int)), &m_mapChache, SLOT(loadTileAsynch(int, QString, QString,int,int,int)), Qt::QueuedConnection);
    connect(&m_mapChache, SIGNAL(tileLoaded(int,QString)), cachedMapProxy, SLOT(tileLoaded(int, QString)), Qt::DirectConnection);
    cachedMapProxy->requestTile(type, mapUrl, zoom, x, y, reply);
    cachedMapProxy->deleteLater();
}


Dispatcher::Dispatcher(QObject *parent) :
    QObject(parent),
    m_requestId(0)
{
    Settings::registerMapTypes(m_mapTypeToUrl);
}
