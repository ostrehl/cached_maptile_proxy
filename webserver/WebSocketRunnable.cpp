#include "WebSocketRunnable.h"
#include "QTimer"
#include "QStringList"
#include "Dispatcher.h"
#include "HttpReply.h"

WebSocketRunnable::WebSocketRunnable(int socket_id, QObject *parent) :
    QObject(parent),
    QRunnable(),
    _socket_descriptor(socket_id)
{
}

void WebSocketRunnable::run()
{
    //create socket
    _socket = new QTcpSocket;
    _loop = new QEventLoop;

    connect(_socket,SIGNAL(readyRead()), this, SLOT(readyRead()), Qt::DirectConnection);
    connect(_socket,SIGNAL(disconnected()), this, SLOT(disconnected()), Qt::DirectConnection);
    connect(_socket,SIGNAL(readChannelFinished()), this, SLOT(doneRecieving()), Qt::DirectConnection);

    if(!_socket->setSocketDescriptor(_socket_descriptor)){
        qDebug() << "error connecting websocket " << _socket_descriptor;
        emit error(_socket->error());
    } else
    {
       //disconnect time out
       QTimer::singleShot(30000, this, SLOT(disconnected()));
      _loop->exec();
    }
    delete _socket;
    delete _loop;
}

void WebSocketRunnable::readyRead()
{
    QStringList tokens = QString(_socket->readLine()).split(QRegExp("[/ \r\n]+"));
    _socket->readAll();

    HttpReply reply;
    Dispatcher::getInstance()->processRequest(tokens, reply);
    //qDebug() << reply;
    _socket->write(reply.toByteArray());
    _socket->flush();
    _socket->close();
}

void WebSocketRunnable::disconnected()
{
      _loop->exit(0);
}

void WebSocketRunnable::doneRecieving()
{

}
