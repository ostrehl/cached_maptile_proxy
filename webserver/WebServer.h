#ifndef WEBSERVER_H
#define WEBSERVER_H

#include <QTcpServer>

class WebServer : public QTcpServer
{
    Q_OBJECT
public:
    explicit WebServer(QObject *parent = 0);
    bool startServer();

signals:

public slots:

protected:
    void incomingConnection(qintptr socketDescriptor);

};

#endif // WEBSERVER_H
