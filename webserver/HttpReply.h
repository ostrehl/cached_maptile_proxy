/**
    @file
    @copyright
        Copyright Olav Strehl 2015
        Distributed under the Boost Software License, Version 1.0.
        (See accompanying file LICENSE_1_0.txt or copy at
        http://www.boost.org/LICENSE_1_0.txt)
*/


#ifndef HTTPREPLY_H
#define HTTPREPLY_H

#include <QByteArray>
#include <QMap>
#include <QJsonObject>

class HttpReply
{
public:
    enum HttpStatusCode {
        OK = 200,
        CREATED = 201,
        BAD_REQUEST = 400,
        UNAUTHORIZED =401,
        FORBIDDEN = 403,
        NOT_FOUND = 404,
        METHOD_NOT_ALLOWED =405,
        INTERNAL_SERVER_ERROR = 500,
        SERVICE_UNAVIALABLE = 503
    };

    HttpReply();
    const QByteArray toByteArray();

    void setStatusCode(HttpStatusCode status);
    void appendHeaderEntry(const QByteArray &headername, const QByteArray &headervalue);
    void appendContent(const QByteArray &content);
    void appendContent(const QJsonObject &content);

private:
    QMap<QByteArray, QByteArray> m_headers;
    QByteArray m_content;
    HttpStatusCode m_statusCode;
};

#endif // HTTPREPLY_H
