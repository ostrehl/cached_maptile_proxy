#include "HttpReply.h"
#include "QString"
#include <QJsonDocument>

HttpReply::HttpReply() :
    m_statusCode(NOT_FOUND)
{
}

const QByteArray HttpReply::toByteArray()
{
    QByteArray reply;
    //build first line bassed on status code
    reply.append("HTTP/1.1 ");
    switch (m_statusCode)
    {
    case OK:
        reply.append("200 OK\r\n");
        break;
    case CREATED:
        reply.append("201 Created\r\n");
        break;
    case BAD_REQUEST:
        reply.append("400 Bad Request\r\n");
        break;
    case UNAUTHORIZED:
        reply.append("401 Unauthorized\r\n");
        break;
    case FORBIDDEN:
        reply.append("403 Forbidden\r\n");
        break;
    case METHOD_NOT_ALLOWED:
        reply.append("405 Method not allowed\r\n");
    case NOT_FOUND:
        reply.append("404 Not Found\r\n");
        break;
    case INTERNAL_SERVER_ERROR:
        reply.append("500 Internal Server Error\r\n");
        break;
    case SERVICE_UNAVIALABLE:
        reply.append("503 Service Unavailable\r\n");
        break;
    }

    //append headers:
    QMap<QByteArray, QByteArray>::Iterator it;
    it = m_headers.begin();
    while (it != m_headers.end())
    {
        reply.append(QByteArray("").append(it.key())
                     .append(": ")
                     .append(it.value())
                     .append("\r\n")
                     );
         it++;
    }

    //add content length and extra empty line if content not empty
    if(m_content.size() > 0)
    {
        reply.append(QByteArray("Content-Length: ")
                         .append(QString::number(m_content.size()))
                         .append("\r\n\r\n")
                         .append(m_content)
                         );
    }
    return reply;
}

void HttpReply::setStatusCode(HttpReply::HttpStatusCode status)
{
    m_statusCode = status;
}

void HttpReply::appendHeaderEntry(const QByteArray &headername, const QByteArray &headervalue)
{
    m_headers.insertMulti(headername, headervalue);
}

void HttpReply::appendContent(const QByteArray &content)
{
    m_content.append(content);
}

void HttpReply::appendContent(const QJsonObject &content)
{
    m_content.append(QJsonDocument(content).toJson());
}
