#include "WebServer.h"
#include "WebSocketRunnable.h"
#include "QThreadPool"
#include "Settings.h"

WebServer::WebServer(QObject *parent) :
    QTcpServer(parent)
{
}

bool WebServer::startServer()
{
    if(this->listen(QHostAddress::Any, Settings::WEB_SERVER_PORT))
    {
        qDebug() << "Web Server started";
        return true;
    } else {
        qDebug() << "Could not start Web Server... port in use?";
        return false;
    }
}

void WebServer::incomingConnection(qintptr socketDescriptor)
{
    //get connection
    WebSocketRunnable *runnable = new WebSocketRunnable(socketDescriptor,this);
    QThreadPool::globalInstance()->start(runnable);
}
