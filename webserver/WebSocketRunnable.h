#ifndef WEBSOCKETRUNNABLE_H
#define WEBSOCKETRUNNABLE_H

#include <QRunnable>
#include <QTcpSocket>
#include <QEventLoop>

class WebSocketRunnable : public QObject, public QRunnable
{
    Q_OBJECT
public:
    explicit WebSocketRunnable(int socket_id, QObject *parent = 0);
    void run();

signals:
    void error(QTcpSocket::SocketError socket_error);

public slots:
    void readyRead();
    void disconnected();
    void doneRecieving();

private:
    QTcpSocket * _socket;
    int _socket_descriptor;
    QEventLoop * _loop;

};

#endif // WEBSOCKETRUNNABLE_H
