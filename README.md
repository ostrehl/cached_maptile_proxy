# README #

### What is this repository for? ###

* Description: 

>Basic implementation of an cached proxy for map tiles (like used from Open Street Map)
>Tiles can be requested from different sources and are cached on hard disk within a cache >directory.

* Version:

>just for testing

### How do I get set up? ###

* Summary of set up

>Qt Creator project

* Configuration

* Dependencies

>Qt 5.x

* How to run tests

not available

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* o.strehl@forst-jagd-edv.de