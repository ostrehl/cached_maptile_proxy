/**
    @file
    @copyright
        Copyright Olav Strehl 2015
        Distributed under the Boost Software License, Version 1.0.
        (See accompanying file LICENSE_1_0.txt or copy at
        http://www.boost.org/LICENSE_1_0.txt)
*/


#include <QCoreApplication>
#include <QThreadPool>
#include "webserver/WebServer.h"
#include "Dispatcher.h"

int main(int argc, char *argv[])
{
    //dispatcher (and so an MapProxy) must life within main thread
    Dispatcher::getInstance();

    //set number of threads for thread pool
    //this are used for asynchronus i/o
    QThreadPool::globalInstance()->setMaxThreadCount(32);

    QCoreApplication a(argc, argv);
    WebServer server;
    server.startServer();

    return a.exec();
}
