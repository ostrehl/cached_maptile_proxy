#ifndef SETTINGS_H
#define SETTINGS_H
#include <QString>

namespace Settings{
    const int WEB_SERVER_PORT = 5566;

    //!maximum numbers of tiles in cache
    const int MAX_TILES_IN_CACHE = 100000;

    //!number of tiles to delete on cahce overflow
    const int DELETE_TILES_ON_OVERFLOW = 5000;


    static void registerMapTypes(QHash<QString, QString> &mapTypeHash)
    {
        mapTypeHash.insert("watercolor", "http://tile.stamen.com/watercolor/%z/%x/%y.jpg");
        mapTypeHash.insert("hikebike", "http://c.tiles.wmflabs.org/hikebike/%z/%x/%y.png");
        mapTypeHash.insert("cycle", "http://a.tile.thunderforest.com/cycle/%z/%x/%y.png");
    }
}

#endif // SETTINGS_H
