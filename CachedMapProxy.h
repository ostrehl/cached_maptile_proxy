#ifndef CACHEDMAPPROXY_H
#define CACHEDMAPPROXY_H

#include <QObject>
#include <webserver/HttpReply.h>
#include <MapCache.h>
#include <QMutex>
#include <QWaitCondition>


class CachedMapProxy : public QObject
{
    Q_OBJECT

public: 
    explicit CachedMapProxy(int requestId, QObject *parent = 0);
    ~CachedMapProxy();

    void requestTile(const QString map_type, const QString map_url, int zoom, int x, int y, HttpReply &reply);

signals:
    void loadTileAsynch(const int requestId, const QString map_type, QString map_url, const int zoom, const int x, const int y);

public slots:
    void tileLoaded(const int reqestId, const QString filename);

protected:


private:
    int m_requestId;
    QMutex m_mutex;
    QWaitCondition m_waitcond;
    QString m_fileName;

};

#endif // CACHEDMAPPROXY_H
